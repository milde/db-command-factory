﻿using Quick.Lib.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;

namespace Quick.Lib.Interface
{
    public interface IDbHelper
    {
        int Execute(string sql);

        int Execute(CommandData command);

        int ExecuteNoneQuery(CommandData command);

        int Delete(string sql);

        int Delete(string sql, IDbDataParameter[] parameters);

        object GetSingle(string sql);

        object GetSingle(string sql, IDbDataParameter[] parameters);

        object GetFieldValue(string sql);

        object GetFieldValue(string sql, IDbDataParameter[] parameters);

        DataTable Query(string sql);

        DataTable Query(string sql, IDbDataParameter[] parameters);

        DataTable Query(CommandData command);
    }
}