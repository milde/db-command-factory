﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quick.Lib.Db
{
    /// <summary>
    /// 参数化对象
    /// </summary>
    public class ParaValue
    {
        /// <summary>
        /// 参数名
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// 参数值操作类型
        /// </summary>
        public ParaOperator valueOperator { get; set; }

        private DbType _type = DbType.String;
        
        /// <summary>
        /// 参数值长度
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// 参数数据库字段类型
        /// </summary>
        public DbType Type { get => _type; set => _type = value; }

        public object Value;

        public ParaValue()
        {
            this.valueOperator = ParaOperator.AUTO;
        }
    }
}