﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Quick.Lib.Db
{
    /// <summary>
    /// 构造好的SQL语句与参数对象
    /// </summary>
    public class CommandData
    {
        public string SQLString;
        public IDbDataParameter[] Parameters;
        public CommandData() { }
        public CommandData(string sql, IDbDataParameter[] parameters)
        {
            this.SQLString = sql;
            if (parameters == null)
                parameters = new IDbDataParameter[0];
            this.Parameters = parameters;
        }

        public T[] GetParameters<T>()
        {
            T[] paras = new T[this.Parameters.Length];
            for (int i = 0; i < this.Parameters.Length; i++)
            {
                paras[i] = (T)this.Parameters[i];
            }
            return paras;
        }

        public MySqlParameter[] GetMySqlParameters()
        {
            MySqlParameter[] paras = new MySqlParameter[this.Parameters.Length];
            for (int i = 0; i < this.Parameters.Length; i++)
            {
                paras[i] = (MySqlParameter)this.Parameters[i];
            }
            return paras;
        }
    }
}
