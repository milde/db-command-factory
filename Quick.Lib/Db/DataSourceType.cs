﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quick.Lib.Db
{
    public enum DataSourceType
    {
        /// <summary>
        /// 未知数据库
        /// </summary>
        UNKNOW,

        /// <summary>
        /// MSSQL SERVER
        /// </summary>
        MsSqlServer,

        /// <summary>s
        /// MYSQL
        /// </summary>
        MySql
    }
}
