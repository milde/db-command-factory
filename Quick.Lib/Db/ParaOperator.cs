﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quick.Lib.Db
{
    /// <summary>
    /// 参数的操作类型
    /// </summary>
    public enum ParaOperator
    {
        /// <summary>
        /// 自动组装，一般 insert / update 语句值等于
        /// </summary>
        AUTO,

        /// <summary>
        /// 自定义语句
        /// </summary>
        CUSCTOM_SQL,

        /// <summary>
        /// 增长 update 语句适用，构建 field = field + @field
        /// </summary>
        INCREMENT_IN_UPDATE,

        /// <summary>
        /// 减少 update 语句适用，构建 field = field - @field
        /// </summary>
        DECREAMENT_IN_UPDATE
    }
}
