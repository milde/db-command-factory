﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Quick.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;
using Quick.Lib.Db;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Quick.Lib.Tests
{
    [TestClass()]
    public class TestCommandFactory
    {

        /// <summary>
        /// 测试转型
        /// </summary>
        [TestMethod()]
        public void testDBParametersTypeAutoImplement()
        {
            CommandFactory factory = new CommandFactory("users", DataSourceType.MySql);
            factory.AddParam("uid", 1);
            factory.AddParam("username", "white");

            CommandData data = factory.GetInsertCommand();

            MySqlParameter[] parameters = data.GetParameters<MySqlParameter>();

            Assert.AreEqual(parameters.GetType(), typeof(MySqlParameter[]));
        }

        /// <summary>
        /// 测试插入数据
        /// </summary>
        [TestMethod()]
        public void testSelectCommand()
        {
            CommandFactory factory = new CommandFactory("users", DataSourceType.MySql);
            CommandData data = factory.GetSelectCommand("*", null, null);
            Assert.AreEqual(data.SQLString, "select * from users where 1=1;");

            factory = new CommandFactory("users", DataSourceType.MySql);
            data = factory.GetSelectCommand("uid,username,age", null, null);
            Assert.AreEqual(data.SQLString, "select uid,username,age from users where 1=1;");

            factory = new CommandFactory("users", DataSourceType.MySql);
            data = factory.GetSelectCommand("uid,username,age", "name=@name", new List<IDbDataParameter>() {
                factory.createParameter("name","white")
            });
            Assert.AreEqual(data.SQLString, "select uid,username,age from users where name=@name;");

            factory = new CommandFactory("users", DataSourceType.MySql);
            data = factory.GetSelectCommand("uid,username,age", "name like '%wang%'");

            Assert.AreEqual(data.SQLString, "select uid,username,age from users where name like '%wang%';");
        }

        /// <summary>
        /// 测试插入数据
        /// </summary>
        [TestMethod()]
        public void testInsertCommand()
        {
            CommandFactory factory = new CommandFactory("users", DataSourceType.MySql);
            factory.AddParam("uid", 1);
            factory.AddParam("username", "white");

            CommandData data = factory.GetInsertCommand();

            Assert.AreEqual(data.SQLString, "insert into users(uid,username) values(@uid,@username);");
        }

        /// <summary>
        /// 测试更新语句
        /// </summary>
        [TestMethod()]
        public void testUpdateCommand()
        {
            CommandFactory factory = new CommandFactory("users", DataSourceType.MySql);
            factory.AddParam("username", "white");

            CommandData data = factory.GetUpdateCommand("where uid=@uid", new List<IDbDataParameter>() {
                factory.createParameter("uid",1)
            });

            Assert.AreEqual(data.SQLString, "update users set username=@username where uid=@uid;");
        }

        /// <summary>
        /// 测试累加更新
        /// </summary>
        [TestMethod()]
        public void testUpdateIncrementCommand()
        {
            CommandFactory factory = new CommandFactory("users", DataSourceType.MySql);
            factory.AddParam("amount", 100, ParaOperator.INCREMENT_IN_UPDATE);

            CommandData data = factory.GetUpdateCommand("where uid=@uid", new List<IDbDataParameter>() {
                factory.createParameter("uid",1)
            });

            Assert.AreEqual(data.SQLString, "update users set amount=amount+100 where uid=@uid;");
        }
    }
}